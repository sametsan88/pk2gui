**Pickit 2 GUI **


Kurulum 

Bu yazılım qt-creator 5.5.1 ile yapıldı.

1 - qtcreator ile pk2gui.pro dosyasını açın ve debug edin. 

yada

2 - package klasörü içinde deb dosyası ile kurulum yapabilirsiniz. Bu paket mint ve ubuntuda test edildi.

yada 

3 - Aşağıdaki komutları vererek kurulum yapabilirsiniz;

a) qmake pk2gui.pro
b) make
   

ENGLISH 
-----------------------

This software is made with qt-creator 5.5.1. 

İnstalling

1 - open pk2gui.pro with qtcreator and debug. 

OR 

2 - There is deb package in  "package" folder. This package is tested linux mint and ubuntu.

OR

3- 
a) qmake pk2gui.pro
b) make
    
